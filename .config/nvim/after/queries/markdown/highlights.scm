(list
  (list_item
    [(list_marker_dot)
     (list_marker_minus)
     (list_marker_plus)
     (list_marker_star)] @text.strong (#set! conceal "◉")))

(list
  (list_item
    (list
      (list_item
        [(list_marker_dot)
         (list_marker_minus)
         (list_marker_plus)
         (list_marker_star)] @text.strong (#set! conceal "•")))))
