vim.filetype.add {
    extension = {
        vs = 'glsl',
        fs = 'glsl',
        gs = 'glsl',
        pd_lua = 'lua',
        pd_luax = 'lua',
        cl = 'opencl',
        make = 'make',
        uproject = 'json',
        uplugin = 'json',
        pde = 'java',
    },
    filename = {
        ['/etc/environment'] = 'confini',
        ['doxyconf'] = 'conf',
        ['~/.config/waybar/config'] = 'json',
    },
}
