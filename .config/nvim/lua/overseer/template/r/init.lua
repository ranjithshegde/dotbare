return {
    'r.single_file',
    'r.pio',
    'r.live-server',
    'r.flutter',
    'r.cxx',
    'r.valgrind',
    'r.wasm',
    'r.cmake',
    'r.openFrameworks',
    'r.cdox',
    'r.dotnet',
}
