return {
    require 'r.plugins.git.gitsigns',
    { 'tpope/vim-fugitive', cmd = { 'G', 'Git', 'Gclog' } },
}
